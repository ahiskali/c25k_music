require 'tempfile'
require 'fileutils'
require_relative "pointer.rb"

class Editor
  attr_reader :pointer, :record

  SAMPLES = {}
  Dir["samples/*"].each { |sample| SAMPLES[sample.scan(/\/(.*)\./)[0][0].to_sym] = sample}
  RECORD_PATH = './record.mp3'

  def initialize(warmup_length)
    @pointer = Pointer.new
    tmp_record = cut(pointer, warmup_length)
    fade_out(tmp_record.path)
    FileUtils.cp tmp_record.path, RECORD_PATH
    tmp_record.close!
  end

  def segment(length, type)
    segment = cut(pointer, length)
    fade_out(segment.path)
    concat(RECORD_PATH, SAMPLES[type])
    concat(RECORD_PATH, segment.path)
  end

  def fade_out(filepath, duration = 3)
    temp_file = temp3
    fade_start = pointer.audio_last_seconds(filepath, duration)
    system "ffmpeg -i #{filepath} -af \"afade=t=out:st=#{fade_start}:d=#{duration}\" #{temp_file.path} -loglevel 8 -y"
    FileUtils.cp temp_file.path, filepath
    temp_file.close!
  end

  protected

    def cut(pointer, length)
      temp_file = temp3
      system "ffmpeg -ss #{pointer.timepoint} -i \"#{pointer.track}\" -t #{length} -c copy \"#{temp_file.path}\" -loglevel 8 -y"
      if  (leftover_length = pointer.forward(length)) > 0
        leftover_filename = cut(pointer, leftover_length)
        concat(temp_file.path, leftover_filename.path)
        leftover_filename.close!
      end
      temp_file
    end

    def temp3
      Tempfile.new(['record', '.mp3'])
    end

    def concat(file1_path, file2_path)
      temp_file = temp3
      system "ffmpeg -i \"concat:#{file1_path}|#{file2_path}\" -c copy \"#{temp_file.path}\" -loglevel 8 -y"
      FileUtils.cp temp_file.path, file1_path
      temp_file.close!
      file1_path
    end

end
