require 'mp3info'

class Pointer
  attr_reader :track, :time, :length

  ZERO_TIME = Time.new(2017, 1, 1)

  def initialize
    @tracks = Dir['music/*.mp3'].shuffle
    @tracks.unshift('samples/opening.mp3')
    next_track
  end

  def audio_last_seconds(filepath, length)
    audio_length(filepath) - length
  end

  def timepoint
    time.strftime("%H:%M:%S")
  end

  def forward(forward_length)
    if @length >= forward_length
      @time += forward_length
      @length -= forward_length
    else
      leftover = forward_length - @length
      next_track
    end
    leftover || 0
  end

  protected

    def next_track
      raise ArgumentError, 'Too few tracks' if @tracks.empty?
      @track = @tracks.shift
      @length = audio_length(@track)
      @time = ZERO_TIME
    end

    def audio_length(filepath)
      Mp3Info.open(filepath).length
    end

end
