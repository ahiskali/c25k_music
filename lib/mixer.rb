require_relative "editor.rb"
class Mixer
  attr_reader :editor

  WARMUP_LENGTH = 5*60 # warmup length in seconds

  def initialize
    @editor = Editor.new(WARMUP_LENGTH)
  end

  def mix
    8.times do |i| # amount of segments
      editor.segment(60, :running) # running segment
      editor.segment(90, :walking) # walking segment
    end

    2.times do |i|
      editor.segment(90, :running)
      editor.segment(5, :walking)
    end
  end

end
