## C25K Mixer

C25K Mixer creates playlists from your music so you can train by [C25K](http://www.c25k.com/) using your audio player instead of a smartphone.

## Requirements

This program uses [ffmpeg](https://www.ffmpeg.org) CLI to edit mp3 files, so it should to be installed and recognized by the shell.

## Usage

Copy your mp3 files to "samples" folder. "Opening.mp3", "running.mp3" and "walking.mp3" filenames are reserved for opening song, signal to run and signal to walk, respectively.

In "mixer.rb" enter your warmup time in seconds and structure of the run session.

## Built With

[ffmpeg](https://www.ffmpeg.org) - for editing mp3 files.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
